'''
Created on 20 Feb, 2017

@author: gaurav
'''
import numpy as np
import pylab as pl
from Backend import dataManipulation as dm

def sig(num):
    if num < 0.:
        return -1
    elif num == 0.0:
        return 0
    else:
        return 1
epochList = [10, 15, 20, 25, 30 , 35, 40, 45, 50]
#url = "http://archive.ics.uci.edu/ml/machine-learning-databases/breast-cancer-wisconsin/breast-cancer-wisconsin.data"
url = "http://archive.ics.uci.edu/ml/machine-learning-databases/ionosphere/ionosphere.data"

def getMyClassVoP(res, x):
    '''Returns class of X as per Voted Perceptron Class definition'''
    num = 0
    for w in res.keys():
        num = num + res[w]*sig(np.dot(w,x))
    return sig(num)

def getMyClassVaP(res, x):
    '''Returns class of X as per Vanilla Class Definition'''
    fx = np.dot(res,x)
    return sig(fx)

def VotedPerceptron(X, Y, epochs):
    '''X is augmented input feature space and Y is corresponding output (1, -1)'''
    #w = np.random.uniform(0,1,(1,X.shape[1]))
    w = np.zeros((1,X.shape[1]))
    c = 1
    res = {}
    it = 1
    while it <= epochs :
        i = 0;
        while i < len(X):
            x = X[i]
            fx = w.dot(x)
            if  Y[i]*sig(fx) <= 0:
                res[tuple(w[0])] = c
                w = w + Y[i]*x
                c = 1
            else:
                c = c + 1
            i = i + 1
        it = it + 1
    return res

def vanillaPerceptron(X, Y, epochs):
    w = np.zeros((1, X.shape[1]))
    it = 1
    while it <= epochs:
        i = 0
        while i<len(X) :
            x = X[i]
            fx = w.dot(x)
            if Y[i]*sig(fx) <= 0:
                w = w + Y[i]*x
            i = i + 1
        it += 1
    return tuple(w[0])

    
def getScore(W, X, Y, getMyClass):
    scr = 0
    for i, x in enumerate(X):
        if Y[i] == getMyClass(W, x):
            scr += 1
    return scr

def Fold10CrossValid(X, Y, Perceptron, getMyClass):
    total_x = X.shape[0]
    test_width = total_x/10
    accuracies = {}
    for epochs in epochList:
        total_scr = 0
        i = 0
        while i < 10 * test_width:
            test_x = X[i:i+test_width, :]
            train_x = np.delete(X, list(range(i, i + test_width)), 0)
            test_y = Y[i:i+test_width]
            train_y = np.delete(Y, list(range(i, i+test_width)), 0)
            W = Perceptron(train_x, train_y, epochs)
            total_scr += getScore(W, test_x, test_y, getMyClass)
            i = i + test_width
        total_scr = total_scr/10.0
        total_scr = total_scr/test_width
        accuracies[epochs] = total_scr
    return accuracies

X, Y = dm.getDataSet(url)
X = dm.normalize_X(X)
pointsVoP = Fold10CrossValid(X, Y, VotedPerceptron, getMyClassVoP)
pointsVaP = Fold10CrossValid(X, Y, vanillaPerceptron, getMyClassVaP)
print pointsVoP
print pointsVaP
voX, voY = dm.getSortedPoints(pointsVoP)
vaX, vaY = dm.getSortedPoints(pointsVaP)
dm.graphPlotting(voX, voY, "green", l="Voted Perceptron")
dm.graphPlotting(vaX, vaY, "red", l= "Vanilla Perceptron")
pl.legend()
pl.show()