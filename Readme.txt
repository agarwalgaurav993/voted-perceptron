Implements:
1. Voted Perceptron
2. Vanilla Perceptron

Output:
It plots the accuracy v/s epochs for each of the above algorithms and returns comparison

N:
VPAlgorithm1.py runs on data-set = "http://archive.ics.uci.edu/ml/machine-learning-databases/breast-cancer-wisconsin/breast-cancer-wisconsin.data"
VPAlgorithm1.py runs on data-set = "http://archive.ics.uci.edu/ml/machine-learning-databases/ionosphere/ionosphere.data"