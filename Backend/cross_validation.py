'''
Created on 21 Feb, 2017

@author: gaurav
'''
def getDataSet(url):
    '''Returns augmented X matrix with 1 as last Column'''
    response = urllib2.urlopen(url)
    data = response.read().split("\n")
    if len(data[-1]) == 0:
        data = data[:-1]
    for line in data:
        if line.find("?") != -1:
            data.remove(line)
    dataMatrix = []
    for line in data:
        dataMatrix.append(line.split(','))
    Y = np.array([row[-1] for row in dataMatrix])
    Y = formatY(Y)
    for row in dataMatrix:
        row[-1] = 1
        for i, col in enumerate(row):
            row[i] = float(row[i])
    dataSet = np.array(dataMatrix)
    return dataSet, Y