'''
Created on 21 Feb, 2017

@author: gaurav
'''
import pylab as pl
import urllib2
import numpy as np
def graphPlotting(X, Y, col, l):
    pl.figure("Epochs - Accuracy")
    pl.xlabel("Epchos")
    pl.ylabel("Accuracy")
    ax = pl.subplot(111)
    ax.scatter(X, Y, color = col)
    pl.plot(X, Y, label = l)
    
    
def formatY(Y):
    #print Y
    vals = set(Y)
    if len(vals) != 2:
        print "Number of Classes NOT 2"
    tdict = dict.fromkeys(set(Y))
    tdict[tdict.keys()[0]] = 1
    tdict[tdict.keys()[1]] = -1
    print tdict
    for i, val in enumerate(Y):
        Y[i] = tdict[val]
    del vals
    return Y

def getDataSet(url):
    '''Returns augmented X matrix with 1 as last Column'''
    response = urllib2.urlopen(url)
    data = response.read().split("\n")
    if len(data[-1]) == 0:
        data = data[:-1]
    for line in data:
        if line.find("?") != -1:
            data.remove(line)
    dataMatrix = []
    for line in data:
        dataMatrix.append(line.split(','))
    Y = [row[-1] for row in dataMatrix]
    Y = np.array(formatY(Y))
    for row in dataMatrix:
        row[-1] = 1
        for i, col in enumerate(row):
            row[i] = float(row[i])
    dataSet = np.array(dataMatrix)
    return dataSet, Y

def normalize_X(X):
    i = 0
    while i < X.shape[1]:
        min_i = min(X[:, i])
        max_i = max(X[:, i])
        if max_i - min_i != 0:
            X[:, i] = (X[:, i] - min_i)/(max_i - min_i)
        return X

def getSortedPoints(points):
    X = []
    Y = []
    for x in sorted(points.keys()):
        X.append(x)
        Y.append(points[x])
    return X, Y